Copyright (C) 2019-2020 - The LineageOS Project

Common device tree for Xiaomi SM6150 based devices
==============

#### Proprietary-files.txt
All unpinned blobs are extracted from [miui_DAVINCI_20.6.30_adad840c8e_10.0.zip](https://bigota.d.miui.com/20.6.30/miui_DAVINCI_20.6.30_adad840c8e_10.0.zip).
